package com.adsplay.wifi.model;

import com.adsplay.delivery.model.DefaultResponse;
import com.adsplay.gateway.model.GwCode;

public class WifiResponse extends DefaultResponse {

    public WifiResponse() {
    }

    @Override
    public void setErrorCode(GwCode arg0) {
        super.setErrorCode(arg0);
    }

    @Override
    public void setMessage(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setReferenceId(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setStatus(int arg0) {
        // TODO Auto-generated method stub

    }

    /**
     * set data by constant field name
     */
    @Override
    public void setData(String fieldName, Object object) {
        super.setData("result", object);
    }
}
