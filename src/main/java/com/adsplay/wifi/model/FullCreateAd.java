package com.adsplay.wifi.model;

public class FullCreateAd extends BasicCreativeAd {

    private static final long serialVersionUID = 1573242134345L;

    private String format = "";

    private String device = "";

    private String platform = "";

    private int mediaCost = 0;

    private String buyType = "";

    private int priority = 0;

    public FullCreateAd(String source, int width, int height) {
        super(source, width, height);
    }

    public FullCreateAd(String source, int width, int height, String adFormat, String device, String platform, int media_cost,
            String buy_type, int priority) {
        super(source, width, height);
        this.format = adFormat;
        this.device = device;
        this.platform = platform;
        this.mediaCost = media_cost;
        this.buyType = buy_type;
        this.priority = priority;
    }

    public String getAdFormat() {
        return format;
    }

    public void setAdFormat(String adFormat) {
        this.format = adFormat;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public int getMedia_cost() {
        return mediaCost;
    }

    public void setMedia_cost(int media_cost) {
        this.mediaCost = media_cost;
    }

    public String getBuy_type() {
        return buyType;
    }

    public void setBuy_type(String buy_type) {
        this.buyType = buy_type;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
