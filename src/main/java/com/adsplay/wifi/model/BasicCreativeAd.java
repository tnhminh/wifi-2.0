package com.adsplay.wifi.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BasicCreativeAd implements Serializable {

    private static final long serialVersionUID = 1565243423423434L;

    @JsonProperty("ad_source")
    private String source = "";

    @JsonProperty("width")
    private int width = 0;

    @JsonProperty("height")
    private int height = 0;

    public BasicCreativeAd(String source, int width, int height) {
        super();
        this.source = source;
        this.width = width;
        this.height = height;
    }

    public String getAdSource() {
        return source;
    }

    public void setAdSource(String adSource) {
        this.source = adSource;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
