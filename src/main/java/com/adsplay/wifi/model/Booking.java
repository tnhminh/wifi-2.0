package com.adsplay.wifi.model;

public class Booking {
    
    private String unit = "";
    
    private int number = 0;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
