package com.adsplay.wifi.model;

import com.adsplay.delivery.model.DefaultRequest;

/**
 * 
 * @author minhtran
 *
 */
public class WifiRequest extends DefaultRequest {

    private String apid = "";

    public String getAccessPointID() {
        return apid;
    }

    public void setAccessPointID(String appId) {
        this.apid = appId;
    }
}
