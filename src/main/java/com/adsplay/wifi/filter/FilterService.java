package com.adsplay.wifi.filter;

import java.util.Set;

public interface FilterService<M> {

    void execute(Set<String> flightIds, M request);

}
