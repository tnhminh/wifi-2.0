package com.adsplay.wifi.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.adsplay.wifi.filter.impl.BookingFilter;
import com.adsplay.wifi.filter.impl.DayHourFilter;
import com.adsplay.wifi.filter.impl.DayWeekFilter;
import com.adsplay.wifi.filter.impl.FrequencyFilter;
import com.adsplay.wifi.filter.impl.TimeFrameFilter;
import com.adsplay.wifi.model.WifiRequest;

public abstract class AbstractFilterHanlder implements FilteringProcess<WifiRequest> {

    // Create new filter name
    protected Set<String> filterTypes = new HashSet<String>();

    protected String[] types;

    protected List<FilterService<WifiRequest>> filterServices;

    protected static final List<String> ALL_TYPE = new ArrayList<String>();
    static {
        ALL_TYPE.add(FilteringProcess.BOOKING_FILTERING);
        ALL_TYPE.add(FilteringProcess.DAYHOUR_FILTERING);
        ALL_TYPE.add(FilteringProcess.DAYWEEK_FILTERING);
        ALL_TYPE.add(FilteringProcess.FREQUENCY_FILTERING);
        ALL_TYPE.add(FilteringProcess.TIMEFRAME_FILTERING);
    }

    /**
     * Define filter service by ordinary
     */
    public void defineTypes() {
        if (types == null)
            return;
        if (types.length > 1) {
            new ArrayList<String>(Arrays.asList(types)).forEach(type -> {
                addFilterOptions(type);
            });
        } else if (types.length == 1) {
            String shortType = types[0];
            switch (shortType) {
            case FilteringProcess.ALL_FILTERING:
                ALL_TYPE.forEach(s -> {
                    addFilterOptions(s);
                });
                break;
            default:
                break;
            }
        }
    }

    protected void initialize() {
        filterServices = new ArrayList<FilterService<WifiRequest>>();
        defineTypes();
        injectAllService();
    }

    private void injectAllService() {
        // Should use list of Filter Service in the future to inject all
        // service at the first time we start this.
        filterTypes.forEach(name -> {
            if (FilteringProcess.BOOKING_FILTERING.equals(name)) {
                filterServices.add(new BookingFilter());
            } else if (FilteringProcess.TIMEFRAME_FILTERING.equals(name)) {
                filterServices.add(new TimeFrameFilter());
            } else if (FilteringProcess.FREQUENCY_FILTERING.equals(name)) {
                filterServices.add(new FrequencyFilter());
            } else if (FilteringProcess.DAYWEEK_FILTERING.equals(name)) {
                filterServices.add(new DayWeekFilter());
            } else if (FilteringProcess.DAYHOUR_FILTERING.equals(name)) {
                filterServices.add(new DayHourFilter());
            }
        });
    }

    /**
     * Using to add filter type to factory
     * 
     * @param filterName
     */
    protected void addFilterOptions(String filterName) {
        filterTypes.add(filterName);
    }

    @Override
    public Set<String> doTarget(Set<String> flightIds, WifiRequest request) {
        Set<String> result = new HashSet<String>(flightIds);
        initialize();
        filterServices.forEach(service -> {
            service.execute(result, request);
        });
        return result;
    }

}
