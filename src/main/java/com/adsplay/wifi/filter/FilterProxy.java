package com.adsplay.wifi.filter;

import java.util.Set;

import com.adsplay.wifi.model.WifiRequest;

/**
 * Using to forward the task to another filter
 * 
 * @author Minh Tran
 *
 */
public class FilterProxy implements FilteringProcess<WifiRequest> {

    private AbstractFilterHanlder filter;

    private String name = "";

    private String[] types;

    public FilterProxy(String filterName, String... filterTypes) {
        this.name = filterName;
        this.types = filterTypes;
    }

    /**
     * Do Targeting
     */
    @Override
    public Set<String> doTarget(Set<String> flightIds, WifiRequest request) {
        if (BASIC.equals(name)) {
            filter = new BasicFilterHandler(types);
        } else if (CUSTOM.equals(name)) {
            filter = new CustomFilterHandler(types);
        }
        return filter.doTarget(flightIds, request);
    }

}
