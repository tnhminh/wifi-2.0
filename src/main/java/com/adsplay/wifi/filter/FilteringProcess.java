package com.adsplay.wifi.filter;

import java.util.Set;

public interface FilteringProcess<T> {

    static final String BOOKING_FILTERING = "booking_filtering";

    static final String FREQUENCY_FILTERING = "frequency_filtering";

    static final String TIMEFRAME_FILTERING = "timeframe_filtering";

    static final String DAYHOUR_FILTERING = "dayhour_filtering";

    static final String DAYWEEK_FILTERING = "dayweek_filtering";

    static final String DEVICE_TYPE_FILTERING = "device_type_filtering";

    static final String DEVICE_OS_FILTERING = "device_os_filtering";

    static final String DEVICE_BRAND_FILTERING = "device_brand_filtering";

    static final String DEVICE_SEGMENT_FILTERING = "device_segment_filtering";

    static final String CATEGORY_FILTERING = "category_filtering";

    static final String AUDIENCE_GENDER_FILTERING = "audience_gender_filtering";

    static final String AUDIENCE_AGE_FILTERING = "audience_age_filtering";

    static final String ALL_FILTERING = "all";

    static final String BASIC = "basic";

    static final String CUSTOM = "custom";

    Set<String> doTarget(Set<String> flightIds, T request);
}
