package com.adsplay.wifi.filter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.adsplay.database.RedisCache;
import com.adsplay.wifi.model.WifiRequest;
import com.adsplay.wifi.utils.RedisKeyUtils;

public class BookingFilter extends AbstractFilter<WifiRequest, Map<String, String>> {

    // Define type of booking
    private static final List<String> BOOKING_NAME_KEYS = new ArrayList<String>();
    static {
        BOOKING_NAME_KEYS.add(RedisKeyUtils.FLIGHT_DAILY_BOOKING_PREFIX);
        BOOKING_NAME_KEYS.add(RedisKeyUtils.FLIGHT_TOTAL_BOOKING_PREFIX);
    }

    private static final String UNIT = "unit";
    private static final String NUMBER = "number";

    @Override
    boolean isSelected(Map<String, String> data) {
        // No targeting by setting up at CMS side
        if (data.isEmpty())
            return true;
        else {
            // Let write here

        }
        return false;
    }

    @Override
    protected Map<String, String> getData(String processName, String requestId, String key) {
        return RedisCache.getInstance().getRetargetingRedis().getCacheAll(processName, requestId, key);
    }

    @Override
    List<String> defineTypeKeys() {
        return BOOKING_NAME_KEYS;
    }

}
