package com.adsplay.wifi.filter.impl;

import java.util.ArrayList;
import java.util.List;

import com.adsplay.database.RedisCache;
import com.adsplay.wifi.model.WifiRequest;
import com.adsplay.wifi.utils.ConvertUtils;
import com.adsplay.wifi.utils.DateTimeUtil;
import com.adsplay.wifi.utils.RedisKeyUtils;

public class DayWeekFilter extends AbstractFilter<WifiRequest, String> {

    // Define type of booking
    private static final List<String> DAYWEEK_TYPE_KEYS = new ArrayList<String>();
    static {
        DAYWEEK_TYPE_KEYS.add(RedisKeyUtils.FLIGHT_DAYWEEK_PREFIX);
    }

    @Override
    boolean isSelected(String data) {
        // No targeting by setting up at CMS side
        if (data.isEmpty())
            return true;
        else {
            // Let write here
            List<String> dayOfWeeks = ConvertUtils.convertStringToList(data);
            return dayOfWeeks.contains(DateTimeUtil.getCurrentDateOfWeek("Asia/Ho_Chi_Minh", true));
        }
    }

    @Override
    protected String getData(String processName, String requestId, String key) {
        return RedisCache.getInstance().getRetargetingRedis().getCache(processName, requestId, key);
    }

    @Override
    List<String> defineTypeKeys() {
        return DAYWEEK_TYPE_KEYS;
    }
}
