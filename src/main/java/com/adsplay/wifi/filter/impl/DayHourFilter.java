package com.adsplay.wifi.filter.impl;

import java.util.ArrayList;
import java.util.List;

import com.adsplay.database.RedisCache;
import com.adsplay.wifi.model.WifiRequest;
import com.adsplay.wifi.utils.ConvertUtils;
import com.adsplay.wifi.utils.DateTimeUtil;
import com.adsplay.wifi.utils.RedisKeyUtils;

public class DayHourFilter extends AbstractFilter<WifiRequest, String> {

    // Define type of booking
    private static final List<String> DAYHOUR_TYPE_KEYS = new ArrayList<String>();
    static {
        DAYHOUR_TYPE_KEYS.add(RedisKeyUtils.FLIGHT_DAYHOUR_PREFIX);
    }

    @Override
    boolean isSelected(String data) {
        // No targeting by setting up at CMS side
        if (data.isEmpty())
            return true;
        else {
            // Let write here
            List<String> hours = ConvertUtils.convertStringToList(data);
            return hours.contains(DateTimeUtil.getCurrentHour("Asia/Ho_Chi_Minh", true));
        }
    }

    @Override
    protected String getData(String processName, String requestId, String key) {
        return RedisCache.getInstance().getRetargetingRedis().getCache(processName, requestId, key);
    }

    @Override
    List<String> defineTypeKeys() {
        return DAYHOUR_TYPE_KEYS;
    }
}
