package com.adsplay.wifi.filter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.adsplay.delivery.model.DefaultRequest;
import com.adsplay.wifi.filter.FilterService;
import com.adsplay.wifi.utils.RedisKeyUtils;

public abstract class AbstractFilter<M extends DefaultRequest, H extends Object> implements FilterService<M> {

    private List<String> targetedFlights;

    protected static final String NO_BOOKING = "no_booking_";

    protected void retainTargetedFlights(Set<String> flightIds) {
        if (!targetedFlights.isEmpty())
            flightIds.retainAll(targetedFlights);
    }

    /**
     * Run and return filtered flights
     * 
     * @param removed
     * @param flightId
     * @return List<String>
     */
    protected String run(String flightId, M request) {
        boolean isSelected = true;
        for (String type : defineTypeKeys()) {
            String key = RedisKeyUtils.buildKey(type, flightId);
            H data = getData(request.getProcessName(), request.getRequestId(), key);
            // do compare total/daily with flight's one
            isSelected = isSelected(data);
            if (!isSelected)
                break;
        }
        return isSelected ? flightId : "";
    }

    protected List<String> prepareAndRun(Set<String> flightIds, M request) {
        List<String> rs = new ArrayList<String>();
        flightIds.forEach(fId -> {
            String selectedFlight = run(fId, request);
            if (!selectedFlight.isEmpty()) {
                rs.add(selectedFlight);
            }
        });
        return rs;
    }

    @Override
    public void execute(Set<String> flightIds, M request) {
        targetedFlights = new ArrayList<String>(prepareAndRun(flightIds, request));
        retainTargetedFlights(flightIds);
    }

    protected boolean validateData(H data) {
        return true;
    }

    abstract boolean isSelected(H data);

    abstract protected H getData(String processName, String requestId, String key);

    abstract List<String> defineTypeKeys();

}
