package com.adsplay.wifi.filter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.adsplay.database.RedisCache;
import com.adsplay.wifi.model.WifiRequest;
import com.adsplay.wifi.utils.RedisKeyUtils;

public class TimeFrameFilter extends AbstractFilter<WifiRequest, Map<String, String>> {

    // Define type of booking
    private static final List<String> TIMEFRAME_TYPE_KEYS = new ArrayList<String>();
    static {
        TIMEFRAME_TYPE_KEYS.add(RedisKeyUtils.FLIGHT_TIME_FRAME_PREFIX);
    }

    private static final String startDate = "start_date";
    private static final String endDate = "end_date";

    @Override
    boolean isSelected(Map<String, String> data) {
        // No targeting by setting up at CMS side
        if (data.isEmpty())
            return true;
        else {
            // Let write here

        }
        return false;
    }

    @Override
    protected Map<String, String> getData(String processName, String requestId, String key) {
        return RedisCache.getInstance().getRetargetingRedis().getCacheAll(processName, requestId, key);
    }

    @Override
    List<String> defineTypeKeys() {
        return TIMEFRAME_TYPE_KEYS;
    }

}
