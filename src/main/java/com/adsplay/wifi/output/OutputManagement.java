package com.adsplay.wifi.output;

import com.adsplay.delivery.output.OutputFactory;
import com.adsplay.delivery.output.OutputProcess;

public class OutputManagement extends OutputFactory {

    public static void main(String[] args) {
    }

    @Override
    protected OutputProcess selectOutput(String outputType) {
        if (outputType.equalsIgnoreCase(HTML)) {
            return new HTMLOutput();
        } else if (outputType.equalsIgnoreCase(JSON)) {
            return new JsonOutput();
        } else if (outputType.equalsIgnoreCase(VAST)) {
            return new VastOutput();
        }
        return null;
    }
}
