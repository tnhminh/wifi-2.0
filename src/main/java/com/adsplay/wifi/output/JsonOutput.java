package com.adsplay.wifi.output;

import com.adsplay.delivery.output.BaseOutput;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonOutput extends BaseOutput {

	@Override
	protected String generateOutput(Object arg0) {
		try {
			return new ObjectMapper().writeValueAsString(arg0);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
