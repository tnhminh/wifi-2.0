package com.adsplay.wifi.service;

import java.io.IOException;

import com.adsplay.wifi.model.WifiRequest;

public interface BaseHandler {

    Object execute(WifiRequest request) throws IOException;

    void setDebuggingService(DebuggingService service);
}
