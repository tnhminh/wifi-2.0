package com.adsplay.wifi.service;

import com.adsplay.delivery.model.DefaultRequest;

public interface BannerDefaultService {
    Object response(DefaultRequest request);
}
