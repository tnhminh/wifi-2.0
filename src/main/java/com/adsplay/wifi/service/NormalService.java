package com.adsplay.wifi.service;

import java.util.List;

import com.adsplay.delivery.model.DefaultRequest;

/**
 * 
 * @author minhtran
 *
 */
public interface NormalService {

    List<Object> doFilterFlights (List<Object> flights, DefaultRequest request);
    
    Object response(List<Object> flights, DefaultRequest request);

}
