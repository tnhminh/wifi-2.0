package com.adsplay.wifi.service;

import com.adsplay.wifi.model.WifiRequest;

public interface RetargetingService<Y> {

    static final String RETARGET_ZONE_FIELD = "zone";

    static final String RETARGET_PLACE_FIELD = "place";

    static final String RETARGET_SEGMENT_FIELD = "segment";

    static final String RETARGET_PROVINCE_FIELD = "province";

    Object response(WifiRequest request);

    void setEventService(EventService service);
}
