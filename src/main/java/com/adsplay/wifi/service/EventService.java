package com.adsplay.wifi.service;

import com.adsplay.delivery.model.DefaultRequest;

/**
 * 
 * @author minhtran
 *
 */
public interface EventService {
    
    boolean isValidMacAddress(String accessPointID);
    
    boolean isEmptyEventCase(DefaultRequest request);
    
    Object response(DefaultRequest request);
    
    void setNormalService(NormalService service);

}
