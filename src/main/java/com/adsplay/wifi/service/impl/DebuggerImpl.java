package com.adsplay.wifi.service.impl;

import java.io.IOException;
import java.util.Map;

import com.adsplay.conf.JsonUtil;
import com.adsplay.database.RedisCache;
import com.adsplay.delivery.model.DefaultRequest;
import com.adsplay.wifi.model.BasicCreativeAd;
import com.adsplay.wifi.model.WifiRequest;
import com.adsplay.wifi.service.BannerDefaultService;
import com.adsplay.wifi.service.DebuggingService;
import com.adsplay.wifi.service.RetargetingService;
import com.adsplay.wifi.utils.IpUtils;
import com.adsplay.wifi.utils.RedisKeyUtils;

/**
 * 
 * @author minhtran
 *
 */
public class DebuggerImpl implements DebuggingService {

    private BannerDefaultService defaultService;

    private RetargetingService<?> retargetingService;

    public DebuggerImpl() {

    }

    /**
     * Validate IP Client
     * 
     * @return true if valid, otherwise return false
     */
    @Override
    public boolean isValidIpClient(String ip) {
        return IpUtils.validate(ip);
    }

    @Override
    public Map<String, String> getAndCheckEmpty(String ip, String processName, String requestId) {
        Map<String, String> result = null;
        if (isValidIpClient(ip)) {
            // Debugger Process here
            String keyDebugger = RedisKeyUtils.buildKey(RedisKeyUtils.DEBUGGER_CLIENT_PREFIX, ip);
            result = RedisCache.getInstance().getDebuggerRedis().getCacheAll(processName, requestId, keyDebugger);
            return result != null ? result : null;
        }
        return result;
    }

    /**
     * 
     * @return Response
     * @throws IOException 
     */
    @Override
    public Object response(WifiRequest request) throws IOException {
        Object response = null;
        String ipClient = request.getIpClient();
        Map<String, String> debuggerData = getAndCheckEmpty(ipClient, request.getProcessName(), request.getRequestId());
        if (debuggerData == null) {
            // Do retargeting service
            response = retargetingService.response(request);
            if (response == null && !request.isNormalCase()) {
                return defaultService.response(request);
            }
        } else {
            // Return Debugger Result here
            request.setDebuggerCase(true);
            // Get data creative by debugging
            return JsonUtil.fromMap(BasicCreativeAd.class, debuggerData);
        }
        return response;
    }

    public BannerDefaultService getDefaultService() {
        return defaultService;
    }

    public void setDefaultService(BannerDefaultService defaultService) {
        this.defaultService = defaultService;
    }

    @Override
    public void setBannerDefaultService(BannerDefaultService service) {
        this.defaultService = service;
    }

    @Override
    public void setRetargetingService(RetargetingService<?> service) {
        this.retargetingService = service;
    }
}
