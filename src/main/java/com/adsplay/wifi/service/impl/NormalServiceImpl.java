package com.adsplay.wifi.service.impl;

import java.util.List;

import com.adsplay.delivery.model.DefaultRequest;
import com.adsplay.wifi.service.NormalService;

public class NormalServiceImpl implements NormalService {

    @Override
    public List<Object> doFilterFlights(List<Object> flights, DefaultRequest request) {
        request.setNormalCase(true);
        return null;
    }

    @Override
    public Object response(List<Object> flights, DefaultRequest request) {
        
        List<Object> filteredFlightIds = doFilterFlights(flights, request);
        // Write to do
        return null;
    }

}
