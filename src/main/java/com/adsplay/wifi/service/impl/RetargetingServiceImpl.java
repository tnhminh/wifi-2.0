package com.adsplay.wifi.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.adsplay.database.RedisCache;
import com.adsplay.wifi.filter.FilterProxy;
import com.adsplay.wifi.filter.FilteringProcess;
import com.adsplay.wifi.model.WifiRequest;
import com.adsplay.wifi.service.EventService;
import com.adsplay.wifi.service.RetargetingService;
import com.adsplay.wifi.utils.ConvertUtils;
import com.adsplay.wifi.utils.RedisKeyUtils;

/**
 * 
 * @author minhtran
 *
 */
public class RetargetingServiceImpl implements RetargetingService<List<String>> {

    // Event case
    private EventService eventService;
    
    // Filter flight
    FilteringProcess<WifiRequest> filterProcess = new FilterProxy(FilteringProcess.BASIC, FilteringProcess.ALL_FILTERING);
    
    private static final List<String> FIELDS_TARGETING = new ArrayList<String>();
    static {
        FIELDS_TARGETING.add(RETARGET_PROVINCE_FIELD);
        FIELDS_TARGETING.add(RETARGET_SEGMENT_FIELD);
        FIELDS_TARGETING.add(RETARGET_PLACE_FIELD);
        FIELDS_TARGETING.add(RETARGET_ZONE_FIELD);
    }
    
    public RetargetingServiceImpl() {
    }

    public boolean isValidUUID(String uuid) {
        String uuidMatch = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}";
        if (uuid.matches(uuidMatch))
            return true;
        else
            return false;
    }

    @Override
    public Object response(WifiRequest request) {
        String processName = request.getProcessName();
        String requestId = request.getRequestId();
        String uuid = request.getUuid();
        List<String> reflightIds = getAndCheckEmpty(uuid, processName, requestId);
        if (reflightIds != null && !reflightIds.isEmpty()) {
            request.setRetargetingCase(true);
            // Do retargeting here
            // 0. Get by MAC AP
            Set<String> mergedFlightIds = doMergedFlightIds(request, processName, requestId, reflightIds);
            if (!mergedFlightIds.isEmpty()){
                // Get User-Agent
                String userAgent = request.getUa();
                // Do filtering here
                filterProcess.doTarget(mergedFlightIds, request);
            }
            // ====================
            return null;
        }
        return eventService.response(request);
    }

    /**
     * Return Merged FlightIds
     * @param request
     * @param processName
     * @param requestId
     * @param retargetFlightIds
     * @return
     */
    protected Set<String> doMergedFlightIds(WifiRequest request, String processName, String requestId, List<String> retargetFlightIds) {
        
        Set<String> result = new HashSet<String>();
        
        String keyAP = RedisKeyUtils.buildKey(RedisKeyUtils.ACCESSPOINT_MAC_PREFIX, request.getAccessPointID());
        Map<String, String> apData = RedisCache.getInstance().getDebuggerRedis().getCacheAll(processName, requestId, keyAP);

        Set<String> uniqueFlightIds = new HashSet<String>();
        for (Entry<String, String> entry : apData.entrySet()) {
            if (FIELDS_TARGETING.contains(entry)) {
                String key = entry.getKey();
                String value = entry.getValue();
                
                if (value == null || value.isEmpty())
                    continue;
                
                String keyValuePair = RedisKeyUtils.buildKeyByMapper(key, value);
                String data = RedisCache.getInstance().getDebuggerRedis().getCache(processName, requestId, keyValuePair);
                
                uniqueFlightIds.addAll(ConvertUtils.convertStringToList(data));
            }
        }
        
        retargetFlightIds.forEach(reFflightId -> {
            uniqueFlightIds.forEach(uFlightId -> {
                if (reFflightId.equals(uFlightId)){
                    result.add(reFflightId);
                }
            });
        });
        
        return result;
    }

    @Override
    public void setEventService(EventService service) {
        this.eventService = service;
    }

    /**
     * getAndCheckEmpty
     * @param uuid
     * @param processName
     * @param requestId
     * @return
     */
    public List<String> getAndCheckEmpty(String uuid, String processName, String requestId) {
        if (isValidUUID(uuid)) {
            String keyRetargeting = RedisKeyUtils.buildKey(RedisKeyUtils.RETARGETING_PREFIX, uuid);
            String value = RedisCache.getInstance().getDebuggerRedis().getCache(processName, requestId, keyRetargeting);
            if (value != null && !value.isEmpty()) {
                return ConvertUtils.convertStringToList(value);
            }
        }
        return null;
    }
}
