package com.adsplay.wifi.service.impl;

import java.util.List;

import com.adsplay.delivery.model.DefaultRequest;
import com.adsplay.wifi.filter.FilterProxy;
import com.adsplay.wifi.filter.FilteringProcess;
import com.adsplay.wifi.model.WifiRequest;
import com.adsplay.wifi.service.EventService;
import com.adsplay.wifi.service.NormalService;

/**
 * 
 * @author minhtran
 *
 */
public class EventServiceImp implements EventService {

    NormalService normalService;

    // Filter flight
    FilteringProcess<WifiRequest> filterProcess = new FilterProxy(FilteringProcess.CUSTOM, FilteringProcess.BOOKING_FILTERING,
            FilteringProcess.DAYHOUR_FILTERING, FilteringProcess.DAYWEEK_FILTERING, FilteringProcess.TIMEFRAME_FILTERING,
            FilteringProcess.FREQUENCY_FILTERING, FilteringProcess.DEVICE_TYPE_FILTERING, FilteringProcess.DEVICE_OS_FILTERING,
            FilteringProcess.DEVICE_BRAND_FILTERING, FilteringProcess.DEVICE_SEGMENT_FILTERING, FilteringProcess.CATEGORY_FILTERING,
            FilteringProcess.AUDIENCE_GENDER_FILTERING, FilteringProcess.AUDIENCE_AGE_FILTERING);

    @Override
    public Object response(DefaultRequest request) {
        List<Object> flightIds = null; // Write next
        if (!isEmptyEventCase(request)) {
            return null;
        }
        return normalService.response(flightIds, request);
    }

    @Override
    public void setNormalService(NormalService service) {
        this.normalService = service;
    }

    @Override
    public boolean isValidMacAddress(String accessPointID) {
        return false;
    }

    @Override
    public boolean isEmptyEventCase(DefaultRequest request) {
        request.setEventCase(true);
        if (isValidMacAddress(((WifiRequest) request).getAccessPointID())) {

        }
        return false;
    }
}
