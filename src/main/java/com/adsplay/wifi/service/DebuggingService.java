package com.adsplay.wifi.service;

import java.io.IOException;
import java.util.Map;

import com.adsplay.wifi.model.WifiRequest;

/**
 * 
 * @author minhtran
 *
 */
public interface DebuggingService {

    boolean isValidIpClient(String ip);

    Map<String, String> getAndCheckEmpty(String ip,  String processName, String requestId);
    
    Object response(WifiRequest defaultRequest) throws IOException;
    
    void setBannerDefaultService(BannerDefaultService service);
    
    void setRetargetingService (RetargetingService<?> service);

}
