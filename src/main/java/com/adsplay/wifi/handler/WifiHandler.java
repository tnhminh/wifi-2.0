package com.adsplay.wifi.handler;

import com.adsplay.wifi.service.DebuggingService;

public class WifiHandler extends DeliveryHandler {

    public WifiHandler() {
    }

    @Override
    public void setDebuggingService(DebuggingService service) {
        this.debugService = service;
    }

}
