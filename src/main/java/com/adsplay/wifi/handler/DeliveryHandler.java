package com.adsplay.wifi.handler;

import java.io.IOException;

import com.adsplay.wifi.model.WifiRequest;
import com.adsplay.wifi.service.BaseHandler;
import com.adsplay.wifi.service.DebuggingService;

/**
 * 
 * @author minhtran
 *
 */
public abstract class DeliveryHandler implements BaseHandler {

    protected DebuggingService debugService;

    public DeliveryHandler() {
    }

    public DeliveryHandler(DebuggingService debugService) {
        this.debugService = debugService;
    }

    /**
     * Main Execute Job
     * @throws IOException 
     */
    @Override
    public Object execute(WifiRequest request) throws IOException {
        return debugService.response(request);
    }
}
