package com.adsplay.wifi.gateway;

import com.adsplay.entity.GwApi;
import com.adsplay.handler.GatewayHandler;
import com.adsplay.model.Service;

import io.vertx.ext.web.RoutingContext;

public class WifiGatewayHandler implements GatewayHandler<String> {

    private WifiProcessManagement deliveryProcessManagement;

    private String processName = "";
    private String type = "";
    private GwApi gwApi;
    private RoutingContext context;

    public WifiGatewayHandler(WifiProcessManagement deliveyrProcessManagement, String processName, String type, GwApi api,
            RoutingContext context) {
        super();
        this.deliveryProcessManagement = deliveyrProcessManagement;
        this.processName = processName;
        this.type = type;
        this.gwApi = api;
        this.context = context;
    }

    @Override
    public void handle(String traceId, String process, Service service, String data) {
        deliveryProcessManagement.reply(type, traceId, processName, gwApi, context, data);
    }

}
