package com.adsplay.wifi.gateway;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.common.logs.WriteLog;
import com.adsplay.common.util.UserAgentUtils;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.Configuration;
import com.adsplay.conf.CoreConfig;
import com.adsplay.conf.GatewayConf;
import com.adsplay.conf.KafkaConfig;
import com.adsplay.database.KafkaService;
import com.adsplay.service.CommonService;
import com.adsplay.service.DynamicClassFactory;
import com.adsplay.service.Init;

import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

/**
 * 
 * @author minhtran
 *
 */
public class WifiServerMain {

    public static void main(String[] args) throws Exception {

        // Read and inject data config to instance
        loadConfig(args);

        GatewayConf gatewayConf = Configuration.getInstance().getGatewayConf();
        KafkaConfig kafkaConf = Configuration.getInstance().getKafkaConfig();
        CoreConfig coreConfig = Configuration.getInstance().getCoreConfig();
        MultiLog.setLogParentFolder(gatewayConf.getLogDir(), coreConfig.getSentryDns(), coreConfig.getSentryEmail(),
                coreConfig.getGrayLog());

        // load all config
        CommonService.init(gatewayConf.getProjectName(), Configuration.getInstance().getCoreConfig(), null,
                Configuration.getInstance().getDataManagerConfig().getPostgres(),
                Configuration.getInstance().getDataManagerConfig().getRedisConfig());

        // Init query or cache for delivery
        if (!Utils.isEmpty(gatewayConf.getInitClass())) {
            DynamicClassFactory.getInstance().createObject(gatewayConf.getInitClass(), Init.class).init(gatewayConf);
        }

        // Create and Deploy Vertx Instance
        Runtime runtime = Runtime.getRuntime();
        int numberOfProcessors = runtime.availableProcessors();
        VertxOptions options = new VertxOptions();
        options.setEventLoopPoolSize(numberOfProcessors)// number of cpu cores
                .setWorkerPoolSize(gatewayConf.getPoolSize()).setHAEnabled(true);
        Vertx vertx = Vertx.vertx(options);
        RouterWifiFactory.getInstance().loadResApi(gatewayConf.getProjectName());
        deployAPI(vertx, HttpDeliveryServer.class.getName(), gatewayConf.getInstances(), gatewayConf.getPoolSize());
        if (null != kafkaConf && kafkaConf.isActive()) {
            KafkaService.getInstance().init(kafkaConf, gatewayConf.getProjectName());
        }
    }

    private static void deployAPI(Vertx vertx, String verticleName, int instances, int poolSize) {
        WriteLog.write("START", "Deploy verticle:[" + verticleName + "]");
        DeploymentOptions gatewayOptions = new DeploymentOptions().setWorker(true).setWorkerPoolName("GW").setInstances(instances)
                .setWorkerPoolSize(poolSize);
        vertx.deployVerticle(verticleName, gatewayOptions, (AsyncResult<String> event) -> {
            if (event.succeeded()) {
                WriteLog.write("START", "Deploy [" + verticleName + "successfully...] " + event.result());
            } else {
                WriteLog.write("START", "Deploy [" + verticleName + "],FAIL...:");
                if (event.cause() != null) {
                    event.cause().printStackTrace();
                    System.exit(0);
                }
            }
        });
    }

    private static String[] configPaths = new String[] { "-conf", "./conf/core_config.json", "-gatewayConf", "./conf/gateway.json",
            "-dataManagerConf", "./conf/data_manager.json" };

    private static void loadConfig(String[] args) throws IOException, Exception {
        String pathGatewayConfig = null;
        String pathCoreConfig = null;
        String pathDataManagerConfig = null;
        args = configPaths;

        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if ("-conf".equalsIgnoreCase(arg) && !Utils.isEmpty(args[i + 1])) {
                pathCoreConfig = args[i + 1];
            }

            if ("-gatewayConf".equalsIgnoreCase(arg) && !Utils.isEmpty(args[i + 1])) {
                pathGatewayConfig = args[i + 1];
            }

            if ("-dataManagerConf".equalsIgnoreCase(arg) && !Utils.isEmpty(args[i + 1])) {
                pathDataManagerConfig = args[i + 1];
            }
        }
        System.out.println("Config folder path:[" + pathCoreConfig + "]");
        System.out.println("DataManager folder path:[" + pathDataManagerConfig + "]");
        System.out.println("Gateway config path:[" + pathGatewayConfig + "]");

        if (Utils.isEmpty(pathGatewayConfig) || Utils.isEmpty(pathCoreConfig) || Utils.isEmpty(pathDataManagerConfig)) {
            System.out.println("************************************************************************************");
            System.out.println("*                                                                                  *");
            System.out.println("*  Vui long set them thong so tao folder 'conf' va dua tat ca file config vao      *");
            System.out.println("*  File config phai co ten theo dinh dang  fb_xxxx.json                            *");
            System.out.println("*  Them thong so:                                                                  *");
            System.out.println("*       -coreConf        [Path]      :    Duong dan toi file cau hinh chung        *");
            System.out.println("*       -gatewayConf     [Path]      :    Duong dan file cau hinh                  *");
            System.out.println("*                                                                                  *");
            System.out.println("*                                                                                  *");
            System.out.println("************************************************************************************");
            System.exit(0);
        }

        String coreConfig = FileUtils.readFileToString(new File(pathCoreConfig), StandardCharsets.UTF_8);
        String gatewayConfig = FileUtils.readFileToString(new File(pathGatewayConfig), StandardCharsets.UTF_8);
        String dataManagerConfig = FileUtils.readFileToString(new File(pathDataManagerConfig), StandardCharsets.UTF_8);
        Configuration.init(coreConfig, gatewayConfig, dataManagerConfig);
        UserAgentUtils.init();
    }

}
