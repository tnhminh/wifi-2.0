/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.adsplay.wifi.gateway;

import java.util.ArrayList;
import java.util.List;

import com.adsplay.common.logs.WriteLog;
import com.adsplay.entity.GwApi;
import com.adsplay.wifi.api.WifiGwApi;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;

/**
 * @author tnhminh
 */
public class RouterWifiFactory extends AbstractVerticle {

    private static final RouterWifiFactory INSTANCE = new RouterWifiFactory();

    private final List<Router> external;

    private final List<Router> admin;

    private final List<GwApi> apis;

    public RouterWifiFactory() {
        external = new ArrayList<>();
        admin = new ArrayList<>();
        apis = new ArrayList<>();
    }

    public static RouterWifiFactory getInstance() {
        return INSTANCE;
    }

    public void addAdmin(Router admin) {
        this.admin.add(admin);
        loadApi(admin, "ADMIN");
    }

    public void addExternal(Router external) {
        this.external.add(external);
        loadApi(external, "EXTERNAL");
    }

    private void loadApi(Router router, String type) {
        apis.stream().filter((restApi) -> (type.equalsIgnoreCase(restApi.getType()))).forEachOrdered((restApi) -> loadApi(router, restApi));
    }

    private void addApiProcess(String moduleName, GwApi restApi, List<Router> routers) {
        routers.forEach(entry -> loadApi(entry, restApi));
    }

    private void loadApi(Router router, GwApi restApi) {
        WriteLog.write("START", "Register API Path :[" + restApi.getPath() + "], Method: [" + restApi.getMethod() + "]");
        router.route(HttpMethod.valueOf(restApi.getMethod()), restApi.getPath())
              .consumes(restApi.getContentType())
              .produces(restApi.getAccepted())
              .handler(routingContext -> WifiProcessManagement.getInstance().process(restApi, routingContext));
    }

    public void loadResApi(String projectName) {
        // Using database to call event
        List<GwApi> restApis = new ArrayList<GwApi>();
        WifiGwApi gwApi = new WifiGwApi();
        gwApi.setId(10);
        gwApi.setName("wifi-delivery");
        gwApi.setProjectName("wifi-delivery-2.0");
        gwApi.setMethod("GET");
        gwApi.setContentType("*/*");
        gwApi.setAccepted("*/*");
        gwApi.setPath("/delivery-wifi");
        gwApi.setExecuteClass("com.adsplay.wifi.process.WifiDeliveryProcess");
        gwApi.setHeader(false);
        gwApi.setDisableEncrypt(true);
        gwApi.setPoolSize(-1);
        gwApi.setType("EXTERNAL");
        gwApi.setDebuggingServiceClass(WifiGwApi.DEBUGGER_CLASS);
        gwApi.setRetargetingServiceClass(WifiGwApi.RETARGETING_SERVICE_CLASS);
        gwApi.setBaseHandlerClass(WifiGwApi.WIFI_HANDLER_CLASS);
        gwApi.setBannerDefault(WifiGwApi.BANNER_DEFAULT_CLASS);
        gwApi.setEventServiceClass(WifiGwApi.EVENT_SERVICE_CLASS);
        gwApi.setNormalServiceClass(WifiGwApi.NORMAL_SERVICE_CLASS);
        restApis.add(gwApi);
        this.apis.addAll(restApis);
    }

}
