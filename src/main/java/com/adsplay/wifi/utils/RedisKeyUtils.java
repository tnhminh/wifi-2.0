package com.adsplay.wifi.utils;

import java.util.HashMap;
import java.util.Map;

public class RedisKeyUtils {

    // Key Mapper
    private static final String ZONE = "zone";

    private static final String PLACE = "place";

    private static final String SEGMENT = "segment";

    private static final String PROVINCE = "province";

    private static final String EVENT = "event";

    private static final String ACCESSPOINT = "accesspoint";

    private static final String RETARGETING = "retargeting";

    private static final String DEBUG = "debug";
    
    // ---------

    private static final String ANY_TYPE = "%s";

    public static final String AD = "_ad_";

    public static final String CREATIVE = "_creative";

    public static final String THIRDTRACKING = "_thirdtracking";

    public static final String AUDIENCEAGE = "_audienceage";

    public static final String AUDIENCEGENDER = "_audiencegender";

    public static final String CATEGORY = "_category";

    public static final String DEVICESEGMENT = "_devicesegment";

    public static final String DEVICEBRAND = "_devicebrand";

    public static final String DEVICEOS = "_deviceos";

    public static final String DEVICETYPE = "_devicetype";

    public static final String DAYWEEK = "_dayweek";

    public static final String HOURDAY = "_hourday";

    public static final String TIMEFRAME = "_timeframe";

    public static final String FREQUENCY = "_frequency";

    public static final String TOTALBOOKING = "_totalbooking";

    public static final String DAILYBOOKING = "_dailybooking";

    public static final String FLIGHT_PREFIX = "flight_" + ANY_TYPE;

    public static final String DEBUGGER_CLIENT_PREFIX = DEBUG + "_" + ANY_TYPE;// +
    // IP_CLIENT_REPLACE;

    public static final String RETARGETING_PREFIX = RETARGETING + "_" + ANY_TYPE; // +
    // UUID_REPLACE;

    public static final String ACCESSPOINT_MAC_PREFIX = ACCESSPOINT + "_" + ANY_TYPE;// +
    // MAC_ADDRESS_REPLACE;

    public static final String EVENT_CASE_PREFIX = EVENT + "_" + ANY_TYPE;// +
    // EVENT_REPLACE;

    public static final String PROVINCE_PREFIX = PROVINCE + "_" + ANY_TYPE;// +
    // PROVINCE_REPLACE;

    public static final String SEGMENT_PREFIX = SEGMENT + "_" + ANY_TYPE;// +
    // SEGMENT_REPLACE;

    public static final String PLACE_PREFIX = PLACE + "_" + ANY_TYPE;// +
    // PLACE_REPLACE;

    public static final String ZONE_PREFIX = ZONE + "_" + ANY_TYPE;// +
                                                                // ZONE_REPLACE;

    public static final String FLIGHT_OBJ = FLIGHT_PREFIX;// + FLIGHT_REPLACE;

    public static final String FLIGHT_DAILY_BOOKING_PREFIX = FLIGHT_OBJ + DAILYBOOKING;

    public static final String FLIGHT_TOTAL_BOOKING_PREFIX = FLIGHT_PREFIX// +
                                                                          // FLIGHT_REPLACE
            + TOTALBOOKING;

    public static final String FLIGHT_FREQUENCY_PREFIX = FLIGHT_PREFIX// +
                                                                      // FLIGHT_REPLACE
            + FREQUENCY;

    public static final String FLIGHT_TIME_FRAME_PREFIX = FLIGHT_PREFIX// +
                                                                       // FLIGHT_REPLACE
            + TIMEFRAME;

    public static final String FLIGHT_DAYHOUR_PREFIX = FLIGHT_PREFIX// +
                                                                    // FLIGHT_REPLACE
            + HOURDAY;

    public static final String FLIGHT_DAYWEEK_PREFIX = FLIGHT_PREFIX// +
                                                                    // FLIGHT_REPLACE
            + DAYWEEK;

    public static final String FLIGHT_DEVICE_TYPE_PREFIX = FLIGHT_PREFIX// +
                                                                        // FLIGHT_REPLACE
            + DEVICETYPE;

    public static final String FLIGHT_DEVICE_OS_PREFIX = FLIGHT_PREFIX// +
                                                                      // FLIGHT_REPLACE
            + DEVICEOS;

    public static final String FLIGHT_DEVICE_BRAND_PREFIX = FLIGHT_PREFIX// +
                                                                         // FLIGHT_REPLACE
            + DEVICEBRAND;

    public static final String FLIGHT_DEVICE_SEGMENT_PREFIX = FLIGHT_PREFIX// +
                                                                           // FLIGHT_REPLACE
            + DEVICESEGMENT;

    public static final String FLIGHT_CATEGORY_PREFIX = FLIGHT_PREFIX// +
                                                                     // FLIGHT_REPLACE
            + CATEGORY;

    public static final String FLIGHT_AUDIENCE_GENDER_PREFIX = FLIGHT_PREFIX// +
                                                                            // FLIGHT_REPLACE
            + AUDIENCEGENDER;

    public static final String FLIGHT_AUDIENCE_AGE_PREFIX = FLIGHT_PREFIX// +
                                                                         // FLIGHT_REPLACE
            + AUDIENCEAGE;

    public static final String FLIGHT_THIRD_TRACKING_PREFIX = FLIGHT_PREFIX// +
                                                                           // FLIGHT_REPLACE
            + THIRDTRACKING;

    public static final String FLIGHT_CREATIVE_PREFIX = FLIGHT_PREFIX// +
                                                                     // FLIGHT_REPLACE
            + CREATIVE;

    public static final String FLIGHT_AD_ID_PREFIX = FLIGHT_PREFIX// +
                                                                  // FLIGHT_REPLACE
            + AD + ANY_TYPE;

    // Mapper 
    private static final Map<String, String> MAPPER = new HashMap<String, String>();
    static {
        MAPPER.put(DEBUG, DEBUGGER_CLIENT_PREFIX);
        MAPPER.put(RETARGETING, RETARGETING_PREFIX);
        MAPPER.put(ACCESSPOINT, ACCESSPOINT_MAC_PREFIX);
        MAPPER.put(EVENT, EVENT_CASE_PREFIX);
        MAPPER.put(PROVINCE, PROVINCE_PREFIX);
        MAPPER.put(SEGMENT, SEGMENT_PREFIX);
        MAPPER.put(PLACE, PLACE_PREFIX);
        MAPPER.put(ZONE, ZONE_PREFIX);
    }
    
    /**
     * Build key by assigning arguments
     * 
     * @param format
     * @param args
     * @return String
     */
    public static String buildKey(String format, Object... args) {
        return String.format(format, args);
    }
    
    public static String buildKeyByMapper(String key, Object... args){
        return buildKey(MAPPER.get(key), args);
    }
}
