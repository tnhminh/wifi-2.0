package com.adsplay.wifi.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConvertUtils {

    public static List<String> convertStringToList(String arString) {
        if (arString.contains("[") && arString.contains("]")) {
            return new ArrayList<String>(Arrays.asList(arString.replace("[", "").replace("]", "").split(",")));
        }
        return null;
    }

}
