package com.adsplay.wifi.process;

import java.io.IOException;

import com.adsplay.conf.JsonUtil;
import com.adsplay.delivery.output.OutputFactory;
import com.adsplay.delivery.process.DeliveryProcess;
import com.adsplay.gateway.model.GwException;
import com.adsplay.model.Service;
import com.adsplay.wifi.model.WifiRequest;
import com.adsplay.wifi.model.WifiResponse;
import com.adsplay.wifi.output.OutputManagement;
import com.adsplay.wifi.service.BaseHandler;

/**
 * @author tnhminh
 * @param <T>
 * @param <R>
 */
public class WifiDeliveryProcess extends DeliveryProcess<WifiRequest, WifiResponse> {

    private BaseHandler baseHandler;

    public WifiDeliveryProcess(String processName) {
        super(processName);
    }

    /**
     * Let describe how to create response
     */
    @Override
    public WifiResponse createResponse() {
        return new WifiResponse();
    }

    /**
     * Let describe how to create request
     */
    @Override
    protected WifiRequest createRequest(String request) throws IOException {
        return JsonUtil.fromString(WifiRequest.class, request);

    }

    /**
     * Let describe how to response result
     */
    @Override
    protected Object dataResponse(Service service, String requestId, WifiRequest request, WifiResponse response, String requestData)
            throws GwException, Exception {
        // Do the main bussiness here
        return baseHandler.execute(request);
    }

    @Override
    public void setHandler(Object obj) {
        if (obj instanceof BaseHandler) {
            baseHandler = (BaseHandler) obj;
        }
    }

    @Override
    public OutputFactory createOutputFactory() throws IOException {
        return new OutputManagement();
    }

}
