package com.adsplay.wifi.render.template;

import org.apache.commons.lang3.StringEscapeUtils;

public class TemplateUtil {

    public static String render(String tplPath, DataModel model) {
        String s = HandlebarsTemplateUtil.execute(tplPath, model);
        return StringEscapeUtils.unescapeHtml4(s);
    }

    public static String render(String tplPath) {
        String s = HandlebarsTemplateUtil.execute(tplPath, new DefaultModel());
        return StringEscapeUtils.unescapeHtml4(s);
    }
}
