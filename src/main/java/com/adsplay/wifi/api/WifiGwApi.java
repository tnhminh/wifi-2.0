package com.adsplay.wifi.api;

import com.adsplay.entity.GwApi;

/**
 * 
 * @author minhtran
 *
 */
public class WifiGwApi extends GwApi {

    public static final String NORMAL_SERVICE_CLASS = "com.adsplay.wifi.service.impl.NormalServiceImpl";

    public static final String EVENT_SERVICE_CLASS = "com.adsplay.wifi.service.impl.EventServiceImp";

    public static final String BANNER_DEFAULT_CLASS = "com.adsplay.wifi.service.impl.BannerDefaultImpl";

    public static final String WIFI_HANDLER_CLASS = "com.adsplay.wifi.handler.WifiHandler";

    public static final String RETARGETING_SERVICE_CLASS = "com.adsplay.wifi.service.impl.RetargetingServiceImpl";

    public static final String DEBUGGER_CLASS = "com.adsplay.wifi.service.impl.DebuggerImpl";
    
    private String debuggingServiceClass = "";

    private String retargetingServiceClass = "";

    private String baseHandlerClass = "";

    private String bannerDefault = "";

    private String eventServiceClass = "";

    private String normalServiceClass = "";

    public WifiGwApi() {

    }

    public String getDebuggingServiceClass() {
        return debuggingServiceClass;
    }

    public void setDebuggingServiceClass(String debuggingServiceClass) {
        this.debuggingServiceClass = debuggingServiceClass;
    }

    public String getRetargetingServiceClass() {
        return retargetingServiceClass;
    }

    public void setRetargetingServiceClass(String retargetingServiceClass) {
        this.retargetingServiceClass = retargetingServiceClass;
    }

    public String getBaseHandlerClass() {
        return baseHandlerClass;
    }

    public void setBaseHandlerClass(String baseHandlerClass) {
        this.baseHandlerClass = baseHandlerClass;
    }

    public String getBannerDefault() {
        return bannerDefault;
    }

    public void setBannerDefault(String bannerDefault) {
        this.bannerDefault = bannerDefault;
    }

    public String getEventServiceClass() {
        return eventServiceClass;
    }

    public void setEventServiceClass(String eventServiceClass) {
        this.eventServiceClass = eventServiceClass;
    }

    public String getNormalServiceClass() {
        return normalServiceClass;
    }

    public void setNormalServiceClass(String normalServiceClass) {
        this.normalServiceClass = normalServiceClass;
    }

}
